#!/bin/bash
################################################################################
# AUTHOR: Mousavi
# EDITED ON: 1400/06/11
# -------------------------------------------------------------------------------
# LOGS:
#
#
################################################################################



# -------------------------------------
# Clone oai repository
# -------------------------------------
git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git ran_folder
cd ran_folder
git switch develop
git fetch --all --tags --prune
git fetch
git pull
# -------------------------------------



# -------------------------------------
# Build eNB
# -------------------------------------
# run inside openairinterface clonned repository.
source oaienv
cd cmake_targets/
./build_oai -c -C -I -i --build_lib all
./build_oai --eNB -V -g RelWithDebInfo -x -w SIMU
./build_oai --eNB -V -g RelWithDebInfo -x -w SIMU --disable-hardware-dependency --disable-deadline --disable-cpu-affinity

# -c: Erase all files to make a rebuild from start
# -C: Erase all files made by previous installation
# -I/--install-external-packages:
# -i/--install-system-files     :
# --build_lib <libraries>       : build optional shared library
# -------------------------------------



# -------------------------------------
# Build UE
# -------------------------------------
# run inside openairinterface clonned repository.
source oaienv
cd cmake_targets/
./build_oai -c -C -I -i --build_lib all
./build_oai --UE --disable-hardware-dependency --disable-deadline --disable-cpu-affinity --ue-timing --ue-trace

# -c: Erase all files to make a rebuild from start
# -C: Erase all files made by previous installation
# -I/--install-external-packages:
# -i/--install-system-files     :
# --build_lib <libraries>       : build optional shared library
# -------------------------------------



# -------------------------------------
# Config files
# -------------------------------------
# eNB Config        : openairinterface5g/targets/PROJECTS/GENERIC-LTE-EPC/CONF/enb.band7.master.conf
# UE Config         : openairinterface5g/ci-scripts/conf_files/ue.nfapi.conf
# UE simcard Config : openairinterface5g/openair3/NAS/TOOLS/ue_eurecom_test_sfr.conf
# UE simcard builder: openairinterface5g/target/bin/conf2uedata
# -------------------------------------



# -------------------------------------
# Alias
# -------------------------------------
echo enbl="cd ~/enb_folder/cmake_targets/ran_build/build/" | sudo tee -a ~/.bashrc
echo u1l="cd ~/ue1/cmake_targets/ran_build/build/" | sudo tee -a ~/.bashrc
echo u12l="cd ~/ue2/cmake_targets/ran_build/build/" | sudo tee -a ~/.bashrc