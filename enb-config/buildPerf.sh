#!/bin/bash
################################################################################
# AUTHOR: Aref
# EDITED ON: 1400/06/30
# -------------------------------------------------------------------------------
# LOGS:
#
#
################################################################################


#├── fast booting
#├─────────────────
sudo ip route add 192.168.61.0/24 via 192.168.1.160 dev wlo1 && sudo ifconfig lo:2 127.0.0.2 netmask 255.0.0.0 up
cd /home/oai/enb_folder/cmake_targets/ran_build/build/; sudo -E ./lte-softmodem -O ../rcc.band7.tm1.nfapi.conf
cd /home/oai/ue_folder/cmake_targets/ran_build/build/; sudo -E ./lte-uesoftmodem -O ../ue.nfapi.conf --L2-emul 3 --num-ues 1
sudo -s
perf record -F 2000 -a -p `pgrep -nx lte-softmodem` --call-graph lbr -g -- sleep 600

source /opt/intel/oneapi/vtune/2021.5.0/env/vars.sh && vtune-gui

ping 8.8.8.8 -I oaitun_ue1


sudo lttng set-session ${exp} && sudo lttng enable-event --kernel --all

exp=rf_init_1ue_30s_t1 && cd ~/Desktop/LTTNG/ && mkdir ${exp} && cd ${exp} && sudo lttng create ${exp} --output=./ && sudo lttng set-session ${exp} && sudo lttng enable-event --kernel rcu_*,sched_*,kmem_*,irq_*,writeback_*,time_*,block_*,syscall_*

sudo lttng add-context --kernel --session=${exp} --type=pid \
--type=procname \
--type=pthread_id \
--type=uid \
--type=perf:cpu:cpu-cycles \
--type=perf:cpu:cycles \
--type=perf:cpu:instructions \
--type=perf:cpu:cache-references \
--type=perf:cpu:cache-misses \
--type=perf:cpu:branch-instructions \
--type=perf:cpu:branches \
--type=perf:cpu:branch-misses \
--type=perf:cpu:faults \
--type=perf:cpu:major-faults \
--type=perf:cpu:minor-faults \
--type=perf:cpu:context-switches \
--type=perf:cpu:cs

sudo lttng start && sleep 30 && sudo lttng stop && sudo lttng destroy && sudo chown -R oai:oai kernel


sudo lttng add-context --kernel -t pid -t procname -t pthread_id -t uid -t perf:cpu:cpu-cycles -t perf:cpu:stalled-cycles-backend -t perf:cpu:cache-misses -t perf:thread:faults -t perf:thread:page-fault -t perf:cpu:page-fault -t perf:cpu:faults


lttng add-context --kernel --type=vtid --type=vpid --type=procname

sudo lttng track --userspace --all


sudo lttng start && sleep 15 && sudo lttng stop && sudo lttng destroy && sudo chown -R oai:oai kernel


sudo perf record -F 99 --call-graph dwarf -p 5987 -g -- sleep 120
sudo perf record -a -F 99 --call-graph lbr -p 4582 -g -- sleep 600
~/Downloads/hotspot-v1.3.0-99-gdf39e78-x86_64.AppImage --debugPaths /usr/lib:/usr/lib/debug:/usr/lib/debug/lib:/usr/lib/debug/lib/x86_64-linux-gnu/:/usr/lib/debug/lib/x86_64-linux-gnu/ld-2.31.so:/usr/lib/:/usr/lib/:~/.debug/usr/lib/:~/.debug



~/Desktop/bionic/tools/perf/perf record -ag -F 99 --call-graph lbr -p `pgrep -nx lte-softmodem` -- sleep 300

cd ~/Desktop/bionic/tools/perf && ./perf script -f | ~/Desktop/FlameGraph/stackcollapse-perf.pl | ~/Desktop/FlameGraph/flamegraph.pl > ~/Desktop/FlameGraph/lbr.svg
LD_LIBRARY_PAATH=/usr/bin/babeltrace/ ./perf data convert -f --all --to-ctf=./CTF_LBR_4ue_s1


###################################
# Adding Interfaces
# ---------------------------------
# sudo ifconfig <dev-name> <ip> netmask <> up
###################################
sudo ifconfig lo:2 127.0.0.2 netmask 255.0.0.0 up 
sudo ifconfig lo:2 127.0.0.2 netmask 255.0.0.0 up



###################################
# Adding Route
# ---------------------------------
# sudo ip route add <network/24> via <Next-HOP-IP> dev <local-NIC-dev>
###################################
sudo ip route add 192.168.61.0/24 via 192.168.1.160 dev wlo1;
ping -c 2 192.168.1.160 && ping -c 4 192.168.61.195

ping -c 4 192.168.61.195	# MME
ping -c 4 192.168.61.194	# HSS
ping -c 4 192.168.61.196	# SPGWC
ping -c 4 192.168.61.197	# SPGWU



###################################
# Run lte_uesoftmodem (nfapi)
# ---------------------------------
# sudo -E ./lte-softmodem -O ../rcc.band7.tm1.nfapi.conf
###################################

cd /home/oai/enb_folder/cmake_targets/ran_build/build/; sudo -E ./lte-softmodem -O ../rcc.band7.tm1.nfapi.conf


cd /home/oai/mosaic5g/oai-ran/cmake_targets/ran_build/build; sudo -E ./lte-softmodem -O ../rcc.band7.tm1.nfapi.conf


cd /home/oai/enb_folder/cmake_targets/ran_build/build/; sudo -E valgrind --tool=callgrind --cache-sim=yes ./lte-softmodem -O ../rcc.band7.tm1.nfapi.conf


cd /home/oai/enb_folder/cmake_targets/ran_build/build/; sudo -E valgrind --tool=callgrind --cache-sim=yes --separate-threads=yes ./lte-softmodem -O ../rcc.band7.tm1.nfapi.conf



# -------------------------------------
# Run lte_uesoftmodem (nfapi)
# -------------------------------------
cd /home/oai/ue_folder/cmake_targets/ran_build/build/; sudo -E ./lte-uesoftmodem -O ../ue.nfapi.conf --L2-emul 3 --num-ues 1

cd /home/oai/ue_folder/targets/bin; ./conf2uedata -c /home/oai/ue_folder/openair3/NAS/TOOLS/ue_eurecom_test_sfr.conf -o /home/oai/ue_folder/targets/bin


cd /home/oai/ue_folder/targets/bin; ./conf2uedata -c /home/oai/ue_folder/openair3/NAS/TOOLS/plmn20895_10Sim.conf -o /home/oai/ue_folder/targets/bin



# -------------------------------------
# Run lte_softmodem (rfsim)
# -------------------------------------
cd /home/oai/openairinterface5g/cmake_targets/ran_build/;
sudo RFSIMULATOR=enb ./lte-softmodem -O ../cn1conf.enb.band7.master.conf --rfsim
sudo RFSIMULATOR=enb ./lte-softmodem -O ../cn2conf.enb.band7.master.conf --rfsim


cd ~/ue_folder/cmake_targets/ran_build/build; sudo RFSIMULATOR=127.0.0.1 ./lte-uesoftmodem -C 2685000000 -r 50 



# -------------------------------------
# PROFILING
# -------------------------------------

sudo -E RFSIMULATOR=enb valgrind --tool=callgrind --cache-sim=yes --separate-threads=yes ./lte-softmodem -O ../cn2conf.enb.band7.master.conf --rfsim
sudo -E RFSIMULATOR=enb perf record -g ./lte-softmodem -O ../cn2conf.enb.band7.master.conf --rfsim
sudo -E RFSIMULATOR=enb perf record -F 99 -a -g -- ./lte-softmodem -O ../cn2conf.enb.band7.master.conf --rfsim
sudo perf record -F 99 -ag --call-graph dwarf -p 5871 -o 5871



sudo ./hotspot-v1.3.0-99-gdf39e78-x86_64.AppImage --debugPaths /usr/lib/debug/ perf5.data




sudo perf record -F 200 -ag -p 5871 --call-graph dwarf -o 5871
sudo perf record -F 200 -ag -p 6090 --call-graph dwarf -o 6090
sudo perf record -F 200 -ag -p 6085 --call-graph dwarf -o 6085
sudo perf record -F 200 -ag -p 6072 --call-graph dwarf -o 6072
sudo perf record -F 200 -ag -p 6071 --call-graph dwarf -o 6071
sudo perf record -F 200 -ag -p 6070 --call-graph dwarf -o 6070
sudo perf record -F 200 -ag -p 6069 --call-graph dwarf -o 6069
sudo perf record -F 200 -ag -p 6064 --call-graph dwarf -o 6064
sudo perf record -F 200 -ag -p 6051 --call-graph dwarf -o 6051
sudo perf record -F 200 -ag -p 6050 --call-graph dwarf -o 6050
sudo perf record -F 200 -ag -p 5926 --call-graph dwarf -o 5926
sudo perf record -F 200 -ag -p 5920 --call-graph dwarf -o 5920
sudo perf record -F 200 -ag -p 5915 --call-graph dwarf -o 5915
sudo perf record -F 200 -ag -p 5909 --call-graph dwarf -o 5909
sudo perf record -F 200 -ag -p 5904 --call-graph dwarf -o 5904
sudo perf record -F 200 -ag -p 5899 --call-graph dwarf -o 5899
sudo perf record -F 200 -ag -p 5892 --call-graph dwarf -o 5892
sudo perf record -F 200 -ag -p 5885 --call-graph dwarf -o 5885
sudo perf record -F 200 -ag -p 5876 --call-graph dwarf -o 5876
sudo perf record -F 200 -ag -p 5872 --call-graph dwarf -o 5872




###################################
# LTTNG Track
###################################
Add specific process attribute values to a Linux kernel domain tracker:

lttng [GENERAL OPTIONS] track --kernel
      (--pid=PID[,PID]… | --vpid=VPID[,VPID]… |
      --uid=UID[,UID]… | --vuid=VUID[,VUID]… |
      --gid=GID[,GID]… | --vgid=VGID[,VGID]… )…

Add all possible process attribute values to a Linux kernel domain tracker:

lttng [GENERAL OPTIONS] track --kernel
      --all (--pid | --vpid | --uid |
      --vuid | --gid | --vgid )…

Add specific process attribute values to a user space domain tracker:

lttng [GENERAL OPTIONS] track --userspace
      (--vpid=VPID[,VPID]… | --vuid=VUID[,VUID]… | --vgid=VGID[,VGID]…)…

Add all possible process attribute values to a user space domain tracker:

lttng [GENERAL OPTIONS] track --userspace
      --all (--vpid | --vgid | --vuid)…
