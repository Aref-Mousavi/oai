#!/bin/bash
################################################################################
# AUTHOR: Mousavi
# EDITED ON: 1400/06/11
# -------------------------------------------------------------------------------
# LOGS:
#
#
################################################################################



# -------------------------------------
# Connectivity
# -------------------------------------
# sudo ip route add <oaitun_enb1>/24 via <enb host ip> dev <UE host NIC ip>



sudo ip route add 10.0.1.1/24 via 192.168.1.162 dev enp1s0f0
ping 10.0.1.1


sudo -E RFSIMULATOR=10.0.1.1 ./lte-uesoftmodem -C 2685000000 -r 25 --rfsim --rfsimulator.serveraddr 10.0.1.1 --log_config.global_log_options level,thread
sudo -E RFSIMULATOR=enb ./lte-softmodem -O ../lte-fdd-basic-sim.conf --rfsim --noS1
# -------------------------------------

# -------------------------------------
# namespace 
# -------------------------------------


#---------------------------#                                        #-----------------------------------------------------------------#
#                           #                                        #                                                                 #
#                           #                                        #            (10.10.1.1) v-ue1 <...> v-def (10.10.2.1)            # space1
#   10.0.1.1 (oaitun_enb)   # <192.168.1.162> ..... <192.168.1.170>  # (enp1s0f0) (10.10.1.2) v-ue2 <...> v-def (10.10.2.1)            # space2
#                           #                                        #            (10.10.1.3) v-ue3 <...> v-def (10.10.2.1)            # space3
#                           #                                        #                                                                 #
#---------------------------#                                        #-----------------------------------------------------------------#

ls /var/run/netns
sudo ip netns exec red ip link

sudo ip netns add space1
sudo ip link add v-ue1 type veth peer name v-def
sudo ip link set v-def netns space1
sudo ip addr add 10.10.1.1/24 dev v-ue1
sudo ip link set v-ue1 up

sudo ip netns exec space1 ip link set dev lo up
sudo ip netns exec space1 ip addr add 10.10.2.1/24 dev v-def
sudo ip netns exec space1 ip link set v-def up
sudo ip netns exec space1 bash


sudo iptables -t nat -A POSTROUTING -s 10.10.0.0/255.255.0.0 -o enp1s0f0 -j MASQUERADE
sudo iptables -A FORWARD -i enp1s0f0 -o v-ue1 -j ACCEPT
sudo iptables -A FORWARD -o enp1s0f0 -i v-ue1 -j ACCEPT