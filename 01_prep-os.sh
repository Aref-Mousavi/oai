#!/bin/bash
################################################################################
# AUTHOR: Mousavi
# EDITED ON: 1400/06/11
# -------------------------------------------------------------------------------
# LOGS:
#
#
################################################################################

sudo add-apt-repository ppa:canonical-hwe-team/backport-iwlwifi
sudo apt-get update
sudo apt install backport-iwlwifi-dkms

# -------------------------------------
# Low latency kernel
# -------------------------------------
sudo apt-cache search --names-only linux-lowlatency
#linux-lowlatency - Complete lowlatency Linux kernel
#linux-lowlatency-hwe-18.04 - Complete lowlatency Linux kernel (dummy transitional package)
#linux-lowlatency-hwe-18.04-edge - Complete lowlatency Linux kernel (dummy transitional package)
#linux-lowlatency-hwe-20.04 - Complete lowlatency Linux kernel
#linux-lowlatency-hwe-20.04-edge - Complete lowlatency Linux kernel

sudo apt-get install linux-lowlatency-hwe-20.04
sudo reboot now


# Checking
uname -r    # before ---> 5.4.0-42-generic
uname -r    # after  ---> 5.4.0-81-lowlatency
grep ^CONFIG_HZ /boot/config-`uname -r`
# CONFIG_HZ_1000=y
# CONFIG_HZ=1000




# -------------------------------------
# Debugging Symbols
# -------------------------------------

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C8CAB6595FDFF622              # keyserver for UBUNTU > 14.04
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ECDCAD72428D7C01              # keyserver for UBUNTU 14.04

codename=$(lsb_release -c | awk  '{print $2}')
sudo tee /etc/apt/sources.list.d/ddebs.list << EOF
deb http://ddebs.ubuntu.com/ ${codename}      main restricted universe multiverse
deb http://ddebs.ubuntu.com/ ${codename}-security main restricted universe multiverse
deb http://ddebs.ubuntu.com/ ${codename}-updates  main restricted universe multiverse
deb http://ddebs.ubuntu.com/ ${codename}-proposed main restricted universe multiverse
EOF

sudo apt-get update
sudo apt-get install linux-image-$(uname -r)-dbgsym
sudo reboot now

# Checking
ls /usr/lib/debug/boot		# vmlinux-XXX-debug



# -------------------------------------
# Power management & Disable CPU Frequency scaling
# -------------------------------------
sudo apt-get install -y i7z cpufrequtils

echo 'GOVERNOR="performance"' | sudo tee -a /etc/default/cpufrequtils

# on UBUNTU > 14.04
sudo systemctl disable ondemand

# on UBUNTU 14.04 LTS
sudo update-rc.d ondemand disable
sudo /etc/init.d/cpufrequtils restart



# disable p-state & c-state
echo 'GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_pstate=disable processor.max_cstate=1 intel_idle.max_cstate=0 idle=poll"' | sudo tee -a /etc/default/grub
echo blacklist intel_powerclamp | sudo tee -a /etc/modprobe.d/blacklist.conf
sudo update-grub
sudo reboot now


# Disable HyperThreading
# Best and recommended way is to disable it from BIOS setting for other ways check https://rigtorp.se/low-latency-guide/ for more info.

cpufreq-info	# Check cpu frequency to be at same range
sudo i7z



# -------------------------------------
# Installing Profiling Tools
# -------------------------------------
sudo apt-get install -y valgrind
sudo apt-get install -y kcachegrind
sudo apt-get install -y linux-tools-common linux-tools-generic linux-tools-`uname -r`
sudo apt-get install gdb

which valgrind; which kcachegrind; which perf; which gdb



# -------------------------------------
# Radio Head files
# -------------------------------------
sudo add-apt-repository ppa:ettusresearch/uhd
sudo apt-get update
sudo apt-get install -y libuhd-dev libuhd003 uhd-host
sudo uhd_usrp_probe	# Checking


# -------------------------------------
# Other softwares
# -------------------------------------
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install git
sudo apt-get install openssh-server subversion 



# -------------------------------------
# Downloading Repository
# -------------------------------------
git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git ran_folder
cd ran_folder
git switch develop
git fetch --all --tags --prune
git fetch
git pull


git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git ue_folder
cd ue_folder
git switch develop
git fetch --all --tags --prune
git fetch
git pull








# -------------------------------------
# Change Username
# -------------------------------------
# Hold SHIFT key during boot => Advance option
usermod -d /home/oai -m -l oai default



# -------------------------------------
# Installing Debugging Kernel
# https://hadibrais.wordpress.com/2017/03/13/installing-ubuntu-kernel-debugging-symbols/
# http://www.alexlambert.com/2017/12/18/kernel-debugging-for-newbies.html
# https://askubuntu.com/questions/197016/how-to-install-a-package-that-contains-ubuntu-kernel-debug-symbols

# https://zablo.net/blog/post/run-and-debug-asp-net-core-rc2-ubuntu-16-04/
# -------------------------------------


# -------------------------------------
# Radio Head files
# https://files.ettus.com/manual/page_install.html
# https://command-not-found.com/uhd_usrp_probe
# -------------------------------------