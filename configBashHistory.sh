#!/bin/bash
################################################################################
# Description:
# Change bash history settings
# https://www.shellhacks.com/tune-command-line-history-bash/
################################################################################
sudo sed -i 's/HISTSIZE=.*/HISTSIZE=10000/' ~/.bashrc
sudo sed -i 's/HISTFILESIZE=.*/HISTFILESIZE=10000/' ~/.bashrc
sudo sed -i "14 i HISTTIMEFORMAT='%F %T    '" ~/.bashrc
sudo sed -i "15 i HISTIGNORE='ls:bg:fg:history:clear'" ~/.bashrc
sudo sed -i '18 i shopt -s cmdhist' ~/.bashrc
